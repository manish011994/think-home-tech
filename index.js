const express = require('express');
const db = require('./util/db');
const  router = require('./route');
const  app = express();

app.use(express.static('public'));
app.use(express.static('node_modules'));
app.use(router);
/*app.get('/',(req,res)=>{
    res.sendFile('index');
});*/

let http =  app.listen(5000);
const socket = require('socket.io');
const io = socket(http);
io.on('connection',(socket)=>{
    console.log('connected');
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    socket.on('connect', function(){
        console.log('user connected');
    });

    socket.on('ON',function(msg){
        console.log(msg);
        io.sockets.emit('trigger','1');
    });
    socket.on('OFF',function(msg){
        console.log(msg);
        io.sockets.emit('trigger','0');
    });


});