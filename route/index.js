const express = require('express');
const home = require('../controller/home');
const login = require('../controller/login');
const router = express.Router();

router.get('/',home.index);
router.get('/login',login.index);
router.post('/login',login.login);
module.exports = router;